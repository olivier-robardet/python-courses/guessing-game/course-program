# Course program

This course request the student to develop and enhance a simple number guessing game.

The game is simple: the program choose a random secret number, and the user (the player) input his guess until the secret number is guessed.
For each of the player input, the program only tells if the given number is too small or too big compared to the secret number.

A full working game is achieved in the very first chapter, using basic Python language syntax.  
Every other chapter add enhancement or new feature. Each improvement is just an excuse to learn more about Python programming.

The student is supposed to create a full new project (a new source code directory) for each chapter, but can copy parts of sources from the previous chapter.  
Creating a new project is required as each chapter may also include teaching about source code structure, refactoring, and Python's tooling (`pip`, `virtualenv`, `pipenv`, ...)

## 1 - Base of the language, control flow and simple types

A simple but functional number guessing game, in a console

**Game features:**

- The game select the secret number with a random between 1 and 100 (both inclusive)
- A loop the player for its guess (the player shoud input an integer)
- If the guess is too small or to big, the game displays a relevant message
- If the guess is good (equals the secret number), the game displays a congratulaiton message, and the program ends
- The player can ask to stop before the end (before he was able to guess the secret number) by inputing `stop`, `quit` or `exit` (all case-insensitive) instead of an integer.

**Python learns:**

- Basic syntax
- Variables
- Simple types (int, string)
- String operation (lower casing)
- Diplays a message using variables' value
- `if`/`elif`/`else`
- `while`/`continue`/`break`
- Error handling
- Imports
- Random number generation using the `random` package

**Steps:**

1. 1 try only: no random number (hardcoded in a variable), no loop: only the input (without error handling) and the if block
2. Add a loop
3. Handle error when converting the user input into int
4. Check user input before convertion to check if known commands to quit
5. Random pick of the secret number using random package

## 2 - Complex types: lists and dictionaries

Add some short term memory to the game.

**Game features:**

- On each prompt, the game shows current the number of guess tries. 
- The prompt should say to guess the number between 1 and 100.
- On victory, the game show how many tries the player used.
- New *commands* to the game. A command is a text input the player can type instead of his integer guess (like `stop`, `quit` or `exit` of the first chapter).
- `inputs` command, that display all the previous guess try of the player in chronological order. e.g.:
```
$ inputs
50
25
35
30
27
```
- `tries` command, that display all guess history with game response, in chronological order. e.g.:
```
$ inputs
50 => Too big!
25 => Too small!
35 => Too big!
30 => Too big!
27 => Too small!
```
- `help` command that show all the commands known by the program and their descriptions

**Python learns:**

- Lists
- Dictionaries
- Mixes of multiple type to create complex data structure

**Steps:**

1. Only the `inputs` command, to learn about list
2. Then the `tries` command, to learn about dictionaries and create a complex structure (list of dictionaries)
3. As a dessert and exercise, the `help` command

## 3 - Functions

Refactor the code into smaller functional parts.

**Game features:**

- An easy mode, enabled when and environment variable `GUESSGAME_EASY` exists and values `1`.  
In that mode, the prompt display the boundaries using closest player inputs.

**Python learns:**

- Function definition
- Function call
- Positional or named arguments
- Functions are values
- Read environment variables
- Multiple expression condition

**Steps:**

1. functions for: 
 - Secret number generation
 - read user input
 - command handling
 - user input evaluation (including convertion to int)
2. Split command handling into multiple functions: 
 - a function for each command
 - a dict of commands and their functions
 - commands handling function
3. Split user input evaluation function, by factorizing history update and invalid number message
4. Add min/max boundaries in a dict, and use it everywhere
5. In user guess evaluation function, if the easy mode is enabled, update the boudaries dict variables

## 4 - Commande line arguments

Make the game configurable.

**Game features:**

- Game is configurable using command line, environment variable or config file.
- Make the previous `GUESSGAME_EASY` fully configurable (add cmdline and config file).
- New configuration to set min and/or max boundaries between which the random number is picked.  
The prompt shoud also use these boundaries.

**Python learns:**

- `argv`
- argsparse and [ConfigArgParse](https://pypi.org/project/ConfigArgParse/) packages
- Manipulate files

**Steps:**

1. First min and max are positional command line arguments ($1 for min, $2 for max), only using argv
2. Then add optional arguments using the argsparse package
3. Get values from environment, if no command line given
4. Add configuration file reading, using a hardcoded config file name (must exists)
5. Configuration file is now optional, and it's path can be given using cmdline or envvar
6. Hardcoded default values

## 5 - Packages

Separate the code into packages.

**Game features:**

- One package for the game engine
- One package for the console UI
- One main package handling configuration
- Add a "console Machine Interface" (console MI), in its one package
- A new configuration item to select the interface to use: `console` or `machine`

**Python learns:**

- Create local package
- Organize code
- Use local package

## 6 - Player program

Make a program that replace the human player to guess the number.

**Game features:**

The game now has an opponent coming for the same universe!

Both programs communicate over stdin/stdout using machine UI. They can be runs connected together using a named pipe:

```shell
mkfifo fifo
guessing_game < fifo | guessing_player > fifo
```

## 7 - Classes and objects

Refactor the guessing_game code using objects.

## 8 - External lib

Add a new text UI using curses.

## 9 - HTTP API

Add a new HTTP API interface

## 10 - Unit tests
